﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FuturaMod1.Models;

// You cannot carry anymore.

namespace FuturaMod1.Controllers
{
    public class PlacesController : Controller
    {
        private PlacesEntities db = new PlacesEntities();
        // static string to store postcode
        private static string thePost = "";
        private static bool isChanged = false;
        private static bool mapSelected = true;
        private static string yourChoice = "nothing";
        private static List<SelectListItem> postcodeList;

        // GET: Start Page
        public ActionResult Start()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Start(string postCode)
        {
            // bind the input of postcode from cshtml
            thePost = postCode;
            return RedirectToAction("Index");
        }

        // GET: Start Page
        public ActionResult StartError()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StartError(string postCode)
        {
            // bind the input of postcode from cshtml
            thePost = postCode;
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            return View();
        }

        // GET: Places
        public ActionResult Index(string postString, string searchString)
        {
            // use viewbag to display postcode and search name in view
            //thePost = postString;
            ViewBag.ShowPost = thePost;
            isChanged = false;
            if (postString != thePost)
            {
                ViewBag.ShowName = "";
                isChanged = true;
                searchString = null;
            }
            else
            {
                ViewBag.ShowName = searchString;
            }

            // check if user changed the postcode
            string getPost = postString;
            var places = from p in db.Places
                         where p.Postcode == thePost
                         select p;
            if (getPost != null) // if the page is postback
            {
                ViewBag.ShowPost = getPost;
                thePost = getPost;
                places = from p in db.Places
                         where p.Postcode == getPost
                         select p;
            }

            // not finding anything, return to start page, display error
            if (!places.Any())
            {
                return RedirectToAction("StartError");
            }

            // handles manual search function
            if (!String.IsNullOrEmpty(searchString))
            {
                places = places.Where(p => p.Name.Contains(searchString) || p.Address.Contains(searchString) || p.PlaceType.Contains(searchString));
            }

            // display error when nothing is there
            ViewBag.Err = "";
            if (!places.Any() && !isChanged)
            {
                ViewBag.Err = "No results found, so we display everything for you instead.";
                places = from p in db.Places
                         where p.Postcode == thePost
                         select p;
            }

            // find the places with matched postcode
            return View(places.ToList());
        }

        // GET: Places/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Places places = db.Places.Find(id);
            if (places == null)
            {
                return HttpNotFound();
            }
            return View(places);
        }

        public ActionResult Home()
        {
            return View();
        }
        
        public ActionResult Explore()
        {
            // Get all the distinct suburb - postcode pairs to use for selecting postcodes on drop down lists
            postcodeList = new List<SelectListItem>();
            var places = (from p in db.Places
                          select new { p.Suburb, p.Postcode }).Distinct();

            // Create the Text to display for the dropdown list and its value
            foreach (var item in places)
            {
                postcodeList.Add(new SelectListItem() { Text = item.Suburb.ToUpper() + " - " + item.Postcode, Value = item.Postcode });
            }

            ViewBag.postcodeList = postcodeList;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Explore(string inBtn, string postcode)
        {
            if (!string.IsNullOrEmpty(postcode) && string.IsNullOrEmpty(inBtn))
            {
                thePost = postcode;
                return RedirectToAction("Index");
            }
            else
            {
                switch (inBtn)
                {
                    case "relax":
                        yourChoice = "Relaxation";
                        return RedirectToAction("IndexNew");
                    case "art":
                        yourChoice = "Art Space";
                        return RedirectToAction("IndexNew");
                    case "cook":
                        yourChoice = "Cooking";
                        return RedirectToAction("IndexNew");
                    case "club":
                        yourChoice = "Club";
                        return RedirectToAction("IndexNew");
                    case "swim":
                        yourChoice = "Swimming Pool";
                        return RedirectToAction("IndexNew");
                    case "park":
                        yourChoice = "Park";
                        return RedirectToAction("IndexNew");
                    case "garden":
                        yourChoice = "Garden";
                        return RedirectToAction("IndexNew");
                    case "market":
                        yourChoice = "Market";
                        return RedirectToAction("IndexNew");
                    case "library":
                        yourChoice = "Library";
                        return RedirectToAction("IndexNew");
                    case "restaurantCafe":
                        yourChoice = "Restaurants and Cafes";
                        return RedirectToAction("IndexNew");
                    case "bakery":
                        yourChoice = "Bakery";
                        return RedirectToAction("IndexNew");
                    case "takeaway":
                        yourChoice = "Takeaway";
                        return RedirectToAction("IndexNew");
                    case "worship":
                        yourChoice = "Place of Worship";
                        return RedirectToAction("IndexNew");
                    case "dance":
                        yourChoice = "Dance";
                        return RedirectToAction("IndexNew");
                    default:
                        return View();
                }
            }
        }

        //public ActionResult HomeIn()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult HomeIn(string inBtn)
        //{
        //    switch (inBtn)
        //    {
        //        case "relax":
        //            yourChoice = "Relaxation";
        //            return RedirectToAction("IndexNew");
        //        case "art":
        //            yourChoice = "Art Space";
        //            return RedirectToAction("IndexNew");
        //        case "cook":
        //            yourChoice = "Cooking";
        //            return RedirectToAction("IndexNew");
        //        case "club":
        //            yourChoice = "Club";
        //            return RedirectToAction("IndexNew");
        //        case "swim":
        //            yourChoice = "Swimming Pool";
        //            return RedirectToAction("IndexNew");
        //        default:
        //            return View();
        //    }
        //}

        //public ActionResult HomeOut()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult HomeOut(string outBtn)
        //{
        //    switch (outBtn)
        //    {
        //        case "park":
        //            yourChoice = "Park";
        //            return RedirectToAction("IndexNew");
        //        case "garden":
        //            yourChoice = "Garden";
        //            return RedirectToAction("IndexNew");
        //        default:
        //            return View();
        //    }
        //}

        public ActionResult IndexNew()
        {
            var places = from p in db.Places
                         where p.PlaceType == yourChoice
                         select p;
            return View(places.ToList());
        }

        public ActionResult Activities()
        {
            return View();
        }


        //// GET: Places/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: Places/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Name,Address,Suburb,State,Postcode,CareType,Provider,OrgType,Remote,Latitude,Longitude,PlaceType,Desc")] Places places)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Places.Add(places);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(places);
        //}

        //// GET: Places/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Places places = db.Places.Find(id);
        //    if (places == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(places);
        //}

        //// POST: Places/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,Name,Address,Suburb,State,Postcode,CareType,Provider,OrgType,Remote,Latitude,Longitude,PlaceType,Desc")] Places places)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(places).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(places);
        //}

        //// GET: Places/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Places places = db.Places.Find(id);
        //    if (places == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(places);
        //}

        //// POST: Places/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Places places = db.Places.Find(id);
        //    db.Places.Remove(places);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
