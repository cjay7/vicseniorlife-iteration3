INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('Alfred Nicholas Gardens','1A Sherbrooke Road','Sherbrooke',3789,-37.8746146,145.3566493,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('George Tindale Memorial Gardens','33 Sherbrooke Road','Sherbrooke',3789,-37.8809422,145.3632706,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('National Rhododendron Gardens','The Georgian Road','Olinda',3788,-37.8517114,145.3663872,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('Pirianda Garden','Hacketts Road','Olinda',3788,-37.8737238,145.3738153,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('Rippon Lea House, Museum & Garden','192 Hotham Street','Elsternwick',3182,-37.8795948,144.9966839,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('Royal Botanic Gardens Cranbourne','1000 Ballarto Road','Cranbourne',3977,-38.1243107,145.2406750,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('Royal Botanic Gardens Melbourne','Birdwood Avenue','South Yarra',3141,-37.9102544,145.0810799,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('Broadmeadows and District Garden Club','Wiseman House','Glenroy',3046,-37.7032917,144.9214593,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('Doncaster Garden Club','Cnr Denhert & Doncaster Road','Doncaster Heights',3109,-37.7875932,145.1461305,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('Geelong West Community Garden','129 Autumn Street','Geelong West',3218,-37.9391546,145.1364016,'Garden');
INSERT INTO dbo.Places(Name,Address,Suburb,Postcode,Latitude,Longitude,PlaceType) VALUES ('Mill Park Garden Club','50 Mill Park Drive','Mill Park',3082,-37.6775400,145.0629723,'Garden');
